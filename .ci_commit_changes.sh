# only committing changes if there are any
if [[ `git status --porcelain` ]]; then
  # Changes
  echo Changes present, committing...
  git add airisc
  git commit -am "Whitelist Bot Update (storage-report:$1)"
  git push
else
  echo No changes, aborting.
fi
