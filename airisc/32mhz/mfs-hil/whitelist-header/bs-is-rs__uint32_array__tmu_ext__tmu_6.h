#ifndef BS-IS-RS__UINT32_ARRAY__TMU_EXT__TMU_6_H
#define BS-IS-RS__UINT32_ARRAY__TMU_EXT__TMU_6_H

#include <stdint.h>

// class labels
#define DATA_GEN_0 0
#define DATA_GEN_1 1
#define DATA_GEN_2 2
#define FUNC_0 100
#define FUNC_1 101
#define FUNC_2 102
#define ISR 200
#define MARKER 201
#define NEGATIVE 255

// max values
#define DATA_GEN_COUNT 3
#define FUNC_COUNT 3

// markers
#define MARKER_SETUP_DONE 0x80009014
#define MARKER_DATA_GEN_CHANGE 0x80009010
#define MARKER_DATA_GEN_START 0x8000900c
#define MARKER_DATA_GEN_DONE 0x80009008
#define MARKER_FUNC_START 0x80009004
#define MARKER_FUNC_DONE 0x80009000

// variables
volatile uint32_t old_address __attribute__ ((section (".tmu_detect_bss"))) = 0; 
volatile uint8_t data_gen_counter __attribute__ ((section (".tmu_detect_bss"))) = 0;
volatile uint8_t data_gen_running __attribute__ ((section (".tmu_detect_bss"))) = 0; // false
volatile uint8_t func_counter __attribute__ ((section (".tmu_detect_bss"))) = 0;
volatile uint8_t func_running __attribute__ ((section (".tmu_detect_bss"))) = 0; // false

// whitelists
uint32_t d0_starts[] __attribute__ ((section (".tmu_detect_data"))) = {0x800046f0};
uint32_t d0_ends[] __attribute__ ((section (".tmu_detect_data"))) = {0x8000473c};
uint32_t d1_starts[] __attribute__ ((section (".tmu_detect_data"))) = {0x800046f0};
uint32_t d1_ends[] __attribute__ ((section (".tmu_detect_data"))) = {0x8000473c};
uint32_t d2_starts[] __attribute__ ((section (".tmu_detect_data"))) = {0x800045d4, 0x800046f0, 0x8000beec, 0x8000befc, 0x8000bf64};
uint32_t d2_ends[] __attribute__ ((section (".tmu_detect_data"))) = {0x80004628, 0x80004738, 0x8000beec, 0x8000bf50, 0x8000bf6c};
uint32_t* data_gen_starts[] __attribute__ ((section (".tmu_detect_data"))) = {d0_starts, d1_starts, d2_starts};
uint8_t data_gen_sizes[] __attribute__ ((section (".tmu_detect_data"))) = {1, 1, 5};
uint32_t* data_gen_ends[] __attribute__ ((section (".tmu_detect_data"))) = {d0_ends, d1_ends, d2_ends};

uint32_t f0_starts[] __attribute__ ((section (".tmu_detect_data"))) = {0x800046f0};
uint32_t f0_ends[] __attribute__ ((section (".tmu_detect_data"))) = {0x8000473c};
uint32_t f1_starts[] __attribute__ ((section (".tmu_detect_data"))) = {0x800046f0, 0x8000bf40};
uint32_t f1_ends[] __attribute__ ((section (".tmu_detect_data"))) = {0x8000473c, 0x8000bf6c};
uint32_t f2_starts[] __attribute__ ((section (".tmu_detect_data"))) = {0x800046f0, 0x8000be94, 0x8000bf08, 0x8000bf5c, 0x8000bf88};
uint32_t f2_ends[] __attribute__ ((section (".tmu_detect_data"))) = {0x8000473c, 0x8000befc, 0x8000bf4c, 0x8000bf6c, 0x8000bf8c};
uint32_t* func_starts[] __attribute__ ((section (".tmu_detect_data"))) = {f0_starts, f1_starts, f2_starts};
uint8_t func_sizes[] __attribute__ ((section (".tmu_detect_data"))) = {1, 2, 5};
uint32_t* func_ends[] __attribute__ ((section (".tmu_detect_data"))) = {f0_ends, f1_ends, f2_ends};

uint32_t isr_starts[] __attribute__ ((section (".tmu_detect_data"))) = {0x80008004, 0x8000be60, 0xc0000a24};
uint8_t isr_size __attribute__ ((section (".tmu_detect_data"))) = 3;
uint32_t isr_ends[] __attribute__ ((section (".tmu_detect_data"))) = {0x80008008, 0x8000bf8c, 0xc0000a24};

// function that checks whether the given address lies within a whitelist given by start_addresses and end_addresses
// returns 0 if address isn't within whitelist and 1 if it is
// uses uint8 for indices => allows for up to 255 separate address spaces
uint8_t __attribute__ ((section (".tmu_detect_text"))) is_legal_access(uint32_t* start_addresses, uint8_t start_size, uint32_t* end_addresses, uint32_t address) { 
    uint8_t result = 0;
    uint8_t left_idx = 0; 
    uint8_t right_idx = start_size - 1;
    uint8_t index = start_size;

    // empty list cannot contain anything
    if (start_size == 0) {
        return 0;
    }

    // address smaller than lower bound of first block
    if (address < start_addresses[left_idx]) {
        return 0;
    }

    // binary search on start_addresses to find closest start address
    uint8_t middle_idx;
    while (left_idx <= right_idx) {
        middle_idx = left_idx + (right_idx - left_idx) / 2;

        // check if present at middle
        if (start_addresses[middle_idx] <= address) {
            if (middle_idx == start_size - 1) {
                // address in last index smaller than current address
                index = middle_idx;
                break;
            } else if (start_addresses[middle_idx + 1] > address) {
                // found closest start
                index = middle_idx;
                break;
            } else {
                // continue search in right side of array
                left_idx = middle_idx + 1;
            }
        } else {
            // continue search in left side of array
            right_idx = middle_idx - 1;
        }
    }
    
    if (index == start_size) {
        // no start address smaller than given address
        return 0;
    }
    
    if (end_addresses[index] >= address) {
        // address within the area given by the found start_address
        result = 1;
    }

    return result;
}

// function that classifies the given address
uint8_t __attribute__ ((section (".tmu_detect_text"))) whitelist_classify(uint32_t address) {

    // if any of the two counters exceeds the maximum value, a different program is running! 
    if (data_gen_counter > DATA_GEN_COUNT || func_counter > FUNC_COUNT) {
        old_address = address;
        return NEGATIVE;
    }

    switch (address) {
        // first check if it is a marker
		case MARKER_SETUP_DONE:
			data_gen_counter = 0;
			data_gen_running = 0; //false
			func_counter = 0;
			func_running = 0; //false
			if (address == old_address) {
				return NEGATIVE;
			}
			old_address = address;
			return MARKER;
		case MARKER_DATA_GEN_CHANGE:
			data_gen_counter++;
			func_counter = 0;
			if (address == old_address) {
				return NEGATIVE;
			}
			old_address = address;
			return MARKER;
		case MARKER_DATA_GEN_START:
			data_gen_running = 1; //true
			if (address == old_address) {
				return NEGATIVE;
			}
			old_address = address;
			return MARKER;
		case MARKER_DATA_GEN_DONE:
			data_gen_running = 0; //false
			if (address == old_address) {
				return NEGATIVE;
			}
			old_address = address;
			return MARKER;
		case MARKER_FUNC_START:
			func_counter++;
			func_running = 1; //true
			if (address == old_address) {
				return NEGATIVE;
			}
			old_address = address;
			return MARKER;
		case MARKER_FUNC_DONE:
			func_running = 0; //false
			if (address == old_address) {
				return NEGATIVE;
			}
			old_address = address;
			return MARKER;
		default:
            // if it isn't a marker check for whitelists according to set status markers
            if (data_gen_running) {
                // may only be in the data_gen whitelist denoted by data_gen_counter, or in the isr whitelist
                if (is_legal_access(data_gen_starts[data_gen_counter-1], data_gen_sizes[data_gen_counter-1], data_gen_ends[data_gen_counter-1], address)) {
                    switch (data_gen_counter) {
						case 1:
							old_address = address;
							return DATA_GEN_0;
						case 2:
							old_address = address;
							return DATA_GEN_1;
						case 3:
							old_address = address;
							return DATA_GEN_2;
						default:
                            old_address = address;
                            return NEGATIVE;
                    }
                } else if (is_legal_access(isr_starts, isr_size, isr_ends, address)) {
                    old_address = address;
                    return ISR;
                } else {
                    old_address = address;
                    return NEGATIVE;
                }
            } else if (func_running) {
                // may only be in the function whitelist denoted by func_counter, or in the isr whitelist
                if (is_legal_access(func_starts[func_counter-1], func_sizes[func_counter-1], func_ends[func_counter-1], address)) {
                    switch (func_counter) {
						case 1:
							old_address = address;
							return FUNC_0;
						case 2:
							old_address = address;
							return FUNC_1;
						case 3:
							old_address = address;
							return FUNC_2;
						default:
                            old_address = address;
                            return NEGATIVE;
                    }
                } else if (is_legal_access(isr_starts, isr_size, isr_ends, address)) {
                    old_address = address;
                    return ISR;
                } else {
                    old_address = address;
                    return NEGATIVE;
                }
            } else {
                // may be the isr at the start where no marker has been seen yet
                if (is_legal_access(isr_starts, isr_size, isr_ends, address)) {
                    old_address = address;
                    return ISR;
                } else {
                    old_address = address;
                    return NEGATIVE;
                }
            }
    }

    old_address = address;
}

#endif /* BS-IS-RS__UINT32_ARRAY__TMU_EXT__TMU_6_H */
