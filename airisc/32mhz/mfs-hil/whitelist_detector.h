#ifndef WHITELIST_DETECTOR_H
#define WHITELIST_DETECTOR_H

// "header" searches the header within the same directory
#include "whitelist-header/bs__uint8_array__tmu_ext__tmu_5.h"
#include "utils.h"
#include <stdint.h>

// struct to bundle second 32-Bit block of tmu-data
// we need the compressed entries
typedef union Transaction_info {
	uint32_t data;
	struct {
		uint8_t write : 1;
		uint8_t size : 3;
		uint8_t error : 1;
		uint16_t compressed_entries : 9;
		uint8_t compression_type : 2;
		uint8_t idle_counter;
		uint8_t waitstate_counter;
	} content;
} transaction_info_t;

// function that gets needed UART elements and calls the whitelist function on them
// returns true (1) if there is a non-whitelisted sample
uint8_t __attribute__ ((section (".tmu_detect_text"))) tmu_detect(uint8_t* classifications, uint16_t* num_clasifications) {
	uint8_t result = 0; 

	// get data
	uint32_t address = tmu_get_data0(TMU_BASE_ADDR); // address
    transaction_info_t info;
	info.data = tmu_get_data1(TMU_BASE_ADDR);	 // rest

	// decompress and classify
	// worst case 512 classifications
	for (uint16_t i = 0; i < info.content.compressed_entries + 1; i++) {
		uint32_t current_address;
		uint8_t size_in_bytes = 1 << info.content.size;
		
		switch (info.content.compression_type) {
			case 1:
				// incremented addresses seen
				current_address = address - (info.content.compressed_entries - i) * size_in_bytes;
				break;
			case 2:
				// decremented addresses seen
				current_address = address + (info.content.compressed_entries - i) * size_in_bytes;
				break;
			default:
				// no compression (3) or same address (0)
				current_address = address;
				break;
		}

		// classify decompressed entry
		classifications[i] = whitelist_classify(current_address); 

		if (classifications[i] == NEGATIVE) {
			result = 1;
		}
	}

	*num_clasifications = info.content.compressed_entries + 1;
	return result;
}

#endif /* WHITELIST_DETECTOR_H */
